#!/bin/bash

# any future command that fails will exit the script
set -e

# Delete the old repo
rm -rf /home/ubuntu/tenancy

# clone the repo again
git clone https://gitlab.com/adex001/tenancy.git
cd tenancy
git checkout ch-deploy-lightsail

#source the nvm file. In an non
#If you are not using nvm, add the actual path like
# PATH=/home/ubuntu/node/bin:$PATH
source /home/ubuntu/.nvm/nvm.sh

pwd

# stop the previous pm2
echo "sudo npm un pm2 -g"
sudo npm un pm2 -g


#pm2 needs to be installed globally as we would be deleting the repo folder.
# this needs to be done only once as a setup script.
echo "sudo npm i pm2 -g"
sudo npm install pm2 -g
# starting pm2 daemon
pm2 status

cd /home/ubuntu/tenancy

#install npm packages
echo "Running npm install"
npm install
npm run build

#Restart the node server
# npm start
pm2 start dist/app.js
